# Dirt Club Discord Bot

Notifications and information about Dirt Rally Club Championships

## About

This bot notifies your channel about the start & end of championships and events. The
Dirt Rally API does not have a token authentication system so a full login is required.
As a result this bot is not designed to run as multiple Dirt Rally accounts to protect 
login information.

## Prerequisites

* Java Runtime Environment
* Clojure

Create a Discord developer application for the bot

### Configuration

* Dirt Rally Login
* Dirt Rally Club Id
* Discord Bot Token
* Discord Channel Id


    {:dirt-api {:club-id CLUB_ID
                :email DIRT_RALLY_LOGIN_EMAIL
                :password DIRT_RALLY_LOGIN_PASSWORD}
     :discord {:token API_TOKEN
               :channel-id CHANNEL_ID}}

## Usage

    clojure -m core -c config.edn