(ns db
  (:require [next.jdbc :as jdbc]))

(def db
  {:dbtype   "sqlite"
   :dbname   "dirt.db"})

(def sent-notifications-table ["
create table if not exists sent_notifications (
  championship_id text,
  event_id text,
  window_type text,
  primary key (championship_id, event_id, window_type)
)"])

(def ds (jdbc/get-datasource db))

(defn get-connection
  []
  (jdbc/get-connection ds))

(defn init-db
  [ds]
  (jdbc/execute! ds sent-notifications-table))

(defn add-notification
  [ds championship-id event-id window-type]
  (jdbc/execute! ds ["insert into sent_notifications values(?, ?, ?)" championship-id event-id (name window-type)]))

(defn get-notification
  [ds championship-id event-id window-type]
  (jdbc/execute-one! ds ["select * from sent_notifications where championship_id = ? and event_id = ? and window_type = ?" championship-id event-id (name window-type)]))