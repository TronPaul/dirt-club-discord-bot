(ns bot
  (:require [clojure.core.async :refer [>! <! go go-loop] :as async]
            [discord.client :as client]
            [discord.types :as types]
            [dirt :as dirt]
            [clojure.string :as string]
            [discord.gateway :as gw]
            [taoensso.timbre :as timbre]
            [db :as db])
  (:import [java.time Instant Duration]
           (java.time.temporal ChronoUnit)))

(def check-interval (* 60 1000))

(def event-notification-types
  {:end {:max-duration (Duration/ofHours 1)
         :message-format "%s event #%d ends in %s!"}
   :start {:max-duration (Duration/ofMinutes 15)
           :message-format "%s event #%d starts in %s!"}})          ; TODO notify people who haven't completed the stage

(defn human-readable-duration
  [^Duration duration]
  (let [parts (first (reduce (fn [[human-durations seconds] unit]
                               (let [unit-seconds (.getSeconds (.getDuration unit))
                                     unit-count (int (/ seconds unit-seconds))
                                     unit-name (.toString unit)]
                                 (if (< 0 unit-count)
                                   [(conj human-durations (format "%d %s" unit-count (if (= 1 unit-count)
                                                                                       (subs unit-name 0 (dec (count unit-name)))
                                                                                       unit-name)))
                                    (- seconds (* unit-count unit-seconds))]
                                   [human-durations seconds])))
                             [[] (.getSeconds duration)]
                             [ChronoUnit/HOURS ChronoUnit/MINUTES ChronoUnit/SECONDS]))]
    (string/join ", and " [(string/join ", " (butlast parts)) (last parts)])))

(defn window-notification
  [now championship event-index event window-type]
  (let [{:keys [max-duration message-format]} (get event-notification-types window-type)]
    (let [duration (Duration/between now (Instant/parse (get-in event [:entryWindow window-type])))]
      (if (and (< (compare duration max-duration) 0) (> (compare duration (Duration/ZERO)) 0))
        (format message-format (:name championship) (inc event-index) (human-readable-duration duration))))))

(defn event-notifications
  [now championship event-index event]
  (let [window-notification-fn (partial window-notification now championship event-index event)]
    (into {} (map #(if-let [notification (window-notification-fn %)]
                     [% notification]) (keys event-notification-types)))))

(defn championship-notifications
  [now championship]
  (let [event-notifications-fn (partial event-notifications now championship)]
    (into {} (map-indexed #(do [(:id %2) (event-notifications-fn %1 %2)]) (:events championship)))))

(defn notifications
  [now championships]
  (let [championship-notifications-fn (partial championship-notifications now)]
    (into {} (map #(do [(:id %) (championship-notifications-fn %)]) championships))))

(defn flatten-map
  ([m]
   (flatten-map [] m []))
  ([prev m result]
   (reduce-kv (fn [acc k v]
                (let [key-path (conj prev k)]
                  (if (associative? v)
                    (flatten-map key-path v acc)
                    (conj acc [key-path v])))) result m)))

(defn notified?
  [ds championship-id event-id window-type]
  (not (nil? (db/get-notification ds championship-id event-id window-type))))

(defn notifier
  [send-chan ds channel check-interval cookie-store club-id]
  (go-loop []
    (doall (map (fn [[[championship-id event-id window-type] notification]]
                  (go
                    (>! send-chan {:channel channel :content notification})
                    (db/add-notification ds championship-id event-id window-type)))
                (filter (fn [[notification-keys _]]
                          (apply (complement notified?) ds notification-keys)) (flatten-map (notifications (Instant/now) (dirt/get-club-championships cookie-store club-id))))))
    (async/timeout check-interval)))

(defn create-client
  [auth-token]
  (let [auth       (types/->SimpleAuth auth-token)
        send-chan  (async/chan)
        recv-chan  (async/chan)
        gateway    (gw/connect-to-gateway auth recv-chan)
        client     (discord.client/->GeneralDiscordClient auth gateway (fn [& args]) send-chan recv-chan)]

    ;; Send the identification message to Discord
    (gw/send-identify gateway)

    ;; Read messages from the send channel and call send-message on them. This allows for
    ;; asynchronous messages sending
    (go-loop []
      (when-let [{:keys [channel content embed tts]} (<! send-chan)]
        (try
          (client/send-message client channel content embed tts)
          (catch Exception e (timbre/errorf "Error sending message: %s" e)))
        (recur)))

    ;; Return the client that we created
    client))

(defn start
  [auth-token channel club-id email password]
  (with-open [client (create-client auth-token)
              db-conn (db/get-connection)]
    (let [cookie-store (clj-http.cookies/cookie-store)]
      (dirt/login! cookie-store email password)
      (notifier (:send-channel client) db-conn channel check-interval cookie-store club-id)
      (while true (Thread/sleep 3000)))))



