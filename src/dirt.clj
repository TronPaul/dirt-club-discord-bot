(ns dirt
  (:require [clj-http.client :as client]
            [clojure.data.json :as json]
            [hickory.core :as hickory]
            [hickory.select :as s])
  (:import (org.apache.http.client.utils URIBuilder DateUtils)
           (org.apache.http.impl.cookie NetscapeDraftSpecProvider)))

(def base-url "https://dirtrally2.com/api/")

(defn cookie-spec
  [http-context]
  (.create (NetscapeDraftSpecProvider. (into-array ^String [DateUtils/PATTERN_RFC1123])) http-context))

(defn xsfrc!
  [cookie-store]
  (client/get "https://dirtrally2.com/api/ClientStore/GetInitialState" {:cookie-store cookie-store :cookie-spec cookie-spec}))

(defn login!
  [cookie-store email password]
  (xsfrc! cookie-store)
  (let [l1 (client/get "https://dirtrally2.com/account/login" {:cookie-store cookie-store :cookie-spec cookie-spec :query-params {"returnUri" "/"} :follow-redirects true})
        form-base-url (.toString (.setParameters (.setPath (URIBuilder. (last (:trace-redirects l1))) "/") []))
        form (first (s/select (s/tag :form) (hickory/as-hickory (hickory/parse (:body l1)))))
        request-verification-token (get-in (first (s/select (and (s/tag :input) (s/attr :name #(= "__RequestVerificationToken" %))) form)) [:attrs :value])
        url (str form-base-url (get-in form [:attrs :action]))]
    (client/post url {:cookie-store cookie-store :cookie-spec cookie-spec :redirect-strategy :lax :follow-redirects true :form-params {"Email" email "Password" password "__RequestVerificationToken" request-verification-token}})
    nil))

(defn get-club
  [cookie-store club-id]
  (xsfrc! cookie-store)
  (json/read-str (:body (client/get (str base-url "Club/" club-id) {:cookie-store cookie-store :cookie-spec cookie-spec})) :key-fn keyword))

(defn get-club-championships
  [cookie-store club-id]
  (xsfrc! cookie-store)
  (json/read-str (:body (client/get (str base-url "Club/" club-id "/championships") {:cookie-store cookie-store :cookie-spec cookie-spec})) :key-fn keyword))

(defn get-club-recent-results
  [cookie-store club-id]
  (xsfrc! cookie-store)
  (json/read-str (:body (client/get (str base-url "Club/" club-id "/recentResults") {:cookie-store cookie-store :cookie-spec cookie-spec})) :key-fn keyword))

(defn get-club-standings
  ([cookie-store club-id]
   (get-club-standings cookie-store club-id 1 100))
  ([cookie-store club-id page page-length]
   (xsfrc! cookie-store)
   (json/read-str (:body (client/get (str base-url "Club/" club-id "/standings/current") {:query-params {"page" page "pageLength" page-length} :cookie-store cookie-store})) :key-fn keyword)))